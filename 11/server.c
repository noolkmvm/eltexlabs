#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

struct Data {
    int f;
    int s;
};

#define MAXPENDING 5

void DieWithError(char *errorMessage);

int main() {
    struct Data data;
    int a = rand()%20,b = rand()%20;
    data.f = a;
    data.s = b;

    int sockfd; //Дескриптор сервера
    int clntSockfd; //Дескриптор клиента
    struct sockaddr_in echoServAddr; //Локальный адресс
    struct sockaddr_in echoClntAddr; //Адресс клиента
    unsigned short echoServPort;     //Порт сервера
    unsigned int clntLen;            /* Length of client address data structure */
    printf("Введите порт: ");
    scanf("%hu", &echoServPort);

    //Создаем сокет с протоколом TCP
    if ((sockfd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) DieWithError("socket() failed");

    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort);      /* Local port */

    //Привязываем сокет к локальному адрессу
    if (bind(sockfd, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) DieWithError("bind() failed");

    //Очередь
    if (listen(sockfd, MAXPENDING) < 0) DieWithError("listen() failed");

    //Размер структуры
    clntLen = sizeof(echoClntAddr);

    //Ждем подключения клиента
    if ((clntSockfd = accept(sockfd, (struct sockaddr *) &echoClntAddr, &clntLen)) < 0) DieWithError("accept() failed");

    printf("Handling client %s\n", inet_ntoa(echoClntAddr.sin_addr));

    while(1) {
        sleep(rand() % 4);
        int recvMsgSize;
        send(clntSockfd, &data, sizeof(data), 0);
        if ((recvMsgSize = recv(clntSockfd, &data, sizeof(data), 0)) < 0) DieWithError("recv() failed");
        if(recvMsgSize > 0) printf("\nКоманда-1: %d \nКоманда-2: %d", data.f, data.s);
        if(data.f <= 0 || data.s <= 0) {
            close(clntSockfd);
            break;
        }
    }
    if(data.f > data.s) printf("Первая команда победила");
    else printf("Вторая команда победила");
    return 0;
}
