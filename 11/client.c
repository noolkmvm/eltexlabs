#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

struct Data {
    int f;
    int s;
};

void DieWithError(char *errorMessage);  /* Error handling function */

int main(int argc, char *argv[]) {
    struct Data data;
    int sock;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr; /* Echo server address */
    unsigned short echoServPort;     /* Echo server port */
    char *servIP = malloc(sizeof(char) * 128);  /* Server IP address (dotted quad) */

    printf("Введите IP-адрес: ");
    scanf("%s", servIP);
    printf("Введите порт: ");
    scanf("%hu", &echoServPort);

    //Создадим сокет
    if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) DieWithError("socket() failed");

    memset(&echoServAddr, 0, sizeof(echoServAddr));     /* Zero out structure */
    echoServAddr.sin_family      = AF_INET;             /* Internet address family */
    echoServAddr.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
    echoServAddr.sin_port        = htons(echoServPort); /* Server port */

    //установим соединение
    if (connect(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0) DieWithError("connect() failed");

    while (1) {
        sleep(rand() % 4);
        int recvMsgSize;
        if ((recvMsgSize = recv(sock, &data, sizeof(data), 0)) < 0) DieWithError("recv() failed");
        if(recvMsgSize > 0) {
            data.f += rand()%5;
            data.f -= rand()%3;
            data.s += rand()%5;
            data.s -= rand()%3;
        }
        send(sock, &data, sizeof(data), 0);
    }

    //close(sock);
}
