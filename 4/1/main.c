#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv){
  FILE *f, *o;
  int temp, n;
  char str[100];
  if(argc < 2){
    fprintf(stderr, "Мало аргументов\n");
    exit(1);
  }else{
    f = fopen(argv[1], "r");
    o = fopen("test.txt", "w");
    temp = atoi(argv[2]);
    while(!feof(f)){
      if(fgets(str,100,f) == NULL) break;
      n = (strlen(str)-1);
      if(n <= temp){
        fputs(str,o);
      }
    }
  }
  fclose(f);
  fclose(o);
  return 0;
}
