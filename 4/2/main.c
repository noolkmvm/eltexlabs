#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char** argv){
  FILE *f, *o,*v;
  char w,t;

  if(argc < 2){
    fprintf(stderr, "Мало аргументов\n");
    exit(1);
  }else{
    f = fopen(argv[1], "r");
    v = fopen(argv[1], "r");
    o = fopen("test.txt", "w");
    fgetc(v);
    int n = (int) argv[2][0];
    while((w = fgetc(f)) != EOF){
      fputc(w,o);
      t = fgetc(v);
      if(t == '\n') fputc(n,o);
    }
  }

  fclose(f);
  fclose(v);
  fclose(o);
  return 0;
}
