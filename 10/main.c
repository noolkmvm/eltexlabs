#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <pthread.h>
#include <sys/resource.h>
#define n 5

int fd[n][2];

void *thread(void *arg) {
    int i = * (int *) arg;
    close(fd[i][0]);
    write(fd[i][1], &i, sizeof(int));
    sleep(rand()%4);
    return NULL;
}

int main() {
    int work, med, vini, status;
    //printf("Введите кол-во пчел: ");
    //scanf("%d", &n);
    printf("Сколько меда изанчально: ");
    scanf("%d", &med);
    printf("Сколько пчелы добывают меда: ");
    scanf("%d", &work);
    printf("Сколько Винни-Пух потребляет: ");
    scanf("%d", &vini);
    printf("\n");
    pid_t pid[n];
    pthread_t th[n];

    for (int i = 0; i < n; i++) {
        pipe(fd[i]);
        pid[i] = fork();

        if(pid[i] == -1) {
            perror("fork");
            exit(1);
        }

        if(pid[i] == 0) {
            while(med > (0-vini)) {
                printf("Starting %dth thread(%d)(%d)\n",i, getpid(), getppid());
                pthread_create(&th[i], NULL, thread, &work);
                pthread_join(th[i],NULL);
                sleep(rand()%5);
                return 0;
            }
        }

    }

    while(med > (0-vini)) {
        for (int i = 0; i < n; i++) {
            close(fd[i][1]);
            int temp;
            read(fd[i][0], &temp, sizeof(int));
            med += temp;
            printf("Пчела (%d)[%d] принесла %d меда, в бочке осталось %d\n", getpid(), getppid(), work, med);
            sleep(2);
            med -= vini;
            printf("Миша съел %d меда, в бочке осталось %d\n", vini, med);
        }
    }

    return 0;
}
