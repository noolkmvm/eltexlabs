#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/msg.h>
#include <string.h>
#include <sys/errno.h>

struct mymsgbuf {
    long mtype;
    int mtext;
};

int msgqid, rc;

void send_message(int qid, struct mymsgbuf *qbuf, long type, int text) {
    qbuf->mtype = type;
    qbuf->mtext = text;

    if((msgsnd(qid, (struct msgbuf *)qbuf, sizeof(int), 0)) ==-1) {
        perror("msgsnd");
        exit(1);
    }
}

void read_message(int qid, struct mymsgbuf *qbuf, long type) {
    qbuf->mtype = type;
    msgrcv(qid, (struct msgbuf *)qbuf, sizeof(int), type, 0);
}

int main() {
    int n, work, gold, status;

    key_t key;
    int qtype = 1;
    struct mymsgbuf qbuf;

    printf("Введите кол-во юнитов: ");
    scanf("%d", &n);
    printf("Сколько золота в шахте: ");
    scanf("%d", &gold);
    printf("Сколько они добывают золота: ");
    scanf("%d", &work);
    printf("\n");
    pid_t pid[n];
    key = ftok(".", 'm');

    if((msgqid = msgget(key, IPC_CREAT|0660)) == -1) {
        perror("msgget");
        exit(1);
    }
    for (int i = 0; i < n; i++) {
        pid[i] = fork();

        if(pid[i] == -1) {
            perror("fork");
            exit(1);
        }

        if(pid[i] == 0) {
            while(gold > 0) {
                if(gold < work) {
                    printf("\nВ шахте осталость %d единиц золота\n", gold);
                    exit(0);
                }

                read_message(msgqid, &qbuf, qtype);
                int temp = qbuf.mtext;

                sleep(rand() % 4);
                temp -= work;
                send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, temp);
                printf("Юнит (%d)[%d] добыл %d единиц золота, в шахте осталось %d\n", getpid(), getppid(), work, temp);
            }
        }
    }

    while(gold > 0) {
        for (int i = 0; i < n; i++) {
            send_message(msgqid, (struct mymsgbuf *)&qbuf, qtype, gold);
            read_message(msgqid, &qbuf, qtype);
            gold = qbuf.mtext;
        }
    }

    if ((rc = msgctl(msgqid, IPC_RMID, NULL)) < 0) {
        perror( strerror(errno) );
        printf("msgctl (return queue) failed, rc=%d\n", rc);
        return 1;
    }


    return 0;
}
