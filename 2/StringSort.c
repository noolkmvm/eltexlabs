#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int n;
char *str[10],*temp;

void input();
void sort();
void output();

int main(){
  input();
  sort();

  int min = strlen(str[0]);
  printf("Длина меньшей строки: %d\n\n", min);

  output();
  return 0;
}

void input() {
  printf("Введите кол-во строк: ");
  scanf("%d", &n);
  printf("\n");
  for (int i = 0; i < n; i++) {
    printf("%d: ", i+1);
    str[i] = (char*)malloc(n);
    scanf("%s", str[i]);
  }
  printf("\n");
}

void sort(){
  int f, s, time = 0;
  for (int i = 1; i < n; i++) {
    f = strlen(str[i-1]);
    s = strlen(str[i]);
    if(f>s){
      temp = str[i-1];
      str[i-1] = str[i];
      str[i] = temp;
      time++;
      i = 0;
    }
  }
  printf("Кол-во перестановок: %d\n\n", time);
  return;
}

void output() {
  for (int i = 0; i < n; i++) {
    printf("Строка %d: %s\n",i+1, str[i]);
    free(str[i]);
  }
}
