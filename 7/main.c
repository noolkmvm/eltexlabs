#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>

int main() {
    int n, work, gold, status, stat;
    printf("Введите кол-во юнитов: ");
    scanf("%d", &n);
    printf("Сколько золота в шахте: ");
    scanf("%d", &gold);
    printf("Сколько они добывают золота: ");
    scanf("%d", &work);
    printf("\n");
    pid_t pid[n];
    int fd[n][2],fd1[n][2];
    for (int i = 0; i < n; i++) {
        pipe(fd[i]);
        pipe(fd1[i]);
        pid[i] = fork();

        if(pid[i] == -1) {
            perror("fork");
            exit(1);
        }

        if(pid[i] == 0) {
            while(gold > work) {
                if(gold < work) {
                    printf("\nВ шахте осталость %d единиц золота\n", gold);
                    exit(0);
                }
                close(fd1[i][1]);
                read(fd1[i][0], &gold, sizeof(int));
                sleep(2);
                int temp = gold - work;

                close(fd[i][0]);
                write(fd[i][1], &temp, sizeof(int));
                printf("Юнит (%d)[%d] добыл %d единиц золота, в шахте осталось %d\n", getpid(), getppid(), work, temp);
                sleep(rand()%4);
            }
        }
        //sleep(2);


    }

    while(gold > 0) {
        for (int i = 0; i < n; i++) {
            close(fd1[i][0]);
            write(fd1[i][1], &gold, sizeof(int));
            close(fd[i][1]);
            int temp;
            read(fd[i][0], &temp, sizeof(int));
            gold = temp;
        }
    }

    return 0;
}
