#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/shm.h>
#include <sys/sem.h>

union semun {
    int val;                  /* значение для SETVAL */
    struct semid_ds *buf;     /* буферы для  IPC_STAT, IPC_SET */
    unsigned short *array;    /* массивы для GETALL, SETALL */
    /* часть, особенная для Linux: */
    struct seminfo *__buf;    /* буфер для IPC_INFO */
};

int main() {
    int n, work, gold, status, shmid, *shm, semid;
    key_t key = IPC_CREAT;
    printf("Введите кол-во юнитов: ");
    scanf("%d", &n);
    printf("Сколько золота в шахте: ");
    scanf("%d", &gold);
    printf("Сколько они добывают золота: ");
    scanf("%d", &work);
    printf("\n");
    pid_t pid[n];

    union semun arg;
    struct sembuf lock_res = {0, -1, 0};	//блокировка ресурса
    struct sembuf rel_res = {0, 1, 0};	//освобождение ресурса

    /* Создадим семафор - для синхронизации работы с разделяемой памятью.*/
    semid = semget(key, 1, 0666 | IPC_CREAT);

    /* Установить в семафоре № 0 (Контроллер ресурса) значение "1" */
    arg.val = 1;
    semctl(semid, 0, SETVAL, arg);

    /* Создадим область разделяемой памяти */
    if ((shmid = shmget(key, sizeof(int), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }

    for (int i = 0; i < n; i++) {
        /* Получим доступ к разделяемой памяти */
        if ((shm = (int *)shmat(shmid, NULL, 0)) == (int *) -1) {
            perror("shmat");
            exit(1);
        }
        pid[i] = fork();

        if(pid[i] == -1) {
            perror("fork");
            exit(1);
        }

        if(pid[i] == 0) {
            while(gold > 0) {
                sleep(rand() % 4);

                /* Заблокируем разделяемую память */
                if((semop(semid, &lock_res, 1)) == -1) {
                    fprintf(stderr, "Lock failed\n");
                    exit(1);
                } else {
                    //printf("Semaphore resources decremented by CHILD(%d) (locked) i=%d\n",getpid(), i);
                    fflush(stdout);
                }
                int temp = *(shm)-work;
                *(shm) = temp;

                printf("Юнит (%d)[%d] добыл %d единиц золота, в шахте осталось %d\n", getpid(), getppid(), work, temp);
                sleep(rand() % 2);
                /* Освободим разделяемую память */
                if((semop(semid, &rel_res, 1)) == -1) {
                    fprintf(stderr, "Unlock failed\n");
                    exit(1);
                } else {
                    //printf("Semaphore resources incremented by CHILD(%d) (unlocked) i=%d\n",getpid(), i);
                    fflush(stdout);
                    sleep(rand() % 2);
                }
            }
            exit(0);
        }
    }

    while(gold > 0) {
        for (int i = 0; i < n; i++) {
            /* Заблокируем разделяемую память */
            if((semop(semid, &lock_res, 1)) == -1) {
                fprintf(stderr, "Lock failed\n");
                exit(1);
            } else {
                //printf("Semaphore resources decremented by CHILD(%d) (locked) i=%d\n",getpid(), i);
                fflush(stdout);
            }
            *(shm) = gold;
            /* Освободим разделяемую память */
            if((semop(semid, &rel_res, 1)) == -1) {
                fprintf(stderr, "Unlock failed\n");
                exit(1);
            } else {
                //printf("Semaphore resources incremented by CHILD(%d) (unlocked) i=%d\n",getpid(), i);
                fflush(stdout);
                sleep(rand() % 8);
            }

            gold = *(shm);

        }
    }


    if (shmdt(shm) < 0) {
        printf("Ошибка отключения\n");
        exit(1);
    }

    /* Удалим созданные объекты IPC */
    if (shmctl(shmid, IPC_RMID, 0) < 0) {
        printf("Невозможно удалить область\n");
        exit(1);
    } else
        printf("Сегмент памяти помечен для удаления\n");
    return 0;
}
