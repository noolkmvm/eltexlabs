#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int number;

struct books{
  char name[20];
  int pages;
  int price;
};

int cmp(const void* val1, const void* val2);

int main(){
  printf("Введите кол-во книг: ");
  scanf("%d", &number);
  struct books** book = malloc(sizeof(struct books*)*number);
  printf("\n");
  for (int i = 0; i < number; i++) {
    book[i] = malloc(sizeof(struct books));
    printf("Введите название книги: ");
    scanf("%s", book[i]->name);
    printf("Введите кол-во страниц: ");
    scanf("%d", &book[i]->pages);
    printf("Введите стоимость: ");
    scanf("%d", &book[i]->price);
    printf("\n");
  }

  qsort(book,number,sizeof(struct books*),cmp);

  printf("----------------------------------------------------------------------\n\n");
  for (int i = 0; i < number; i++) {
    printf("Название %d книги: %s\n",i+1, book[i]->name);
    printf("Кол-во страниц: %d\n", book[i]->pages);
    printf("Cтоимость: %d\n", book[i]->price);
    printf("\n");
    free(book[i]);
  }
  free(book);
  return 0;
}

int cmp(const void* val1, const void* val2){
  struct books* a = *(struct books**)val1;
  struct books* b = *(struct books**)val2;
  return strcmp((a->name),(b->name));
}
