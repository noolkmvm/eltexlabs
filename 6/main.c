#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <fcntl.h>

int main() {
    int fd = open("file.txt", O_RDWR|O_EXCL);
    int n, work, status, count;
    char buff[64];
    printf("Введите кол-во юнитов: ");
    scanf("%d", &n);
    printf("Сколько они добывают золота: ");
    scanf("%d", &work);
    printf("\n");
    pid_t pid[n];
    count = read(fd, buff, 64);
    int gold = atoi(buff);
    close(fd);

    for (int i = 0; i < n; i++) {
        pid[i] = fork();

        if(pid[i] == -1) {
            perror ("fork");
            exit(1);
        }

        if(pid[i] == 0) {
            while(gold > work) {
                if(gold < work) {
                    printf("\nВ шахте осталость %d единицы золота\n", gold);
                    exit(0);
                }
                fd = open("file.txt", O_RDWR|O_EXCL);
                if(lockf(fd, F_LOCK, 0) < 0)  perror("SETLOCK");
                sleep(2);
                read(fd, buff, 64);
                int temp = atoi(buff);

                printf("Юнит (%d)[%d] вошел в шахту\n", getpid(), getppid());
                temp -= work;
                gold = temp;
                sprintf(buff, "%d", temp);
                lseek (fd, 0, 0);
                write(fd,buff,3);
                printf("Юнит (%d)[%d] вышел из шахты, добыл %d единиц золота | В шахте осталось %d\n\n", getpid(), getppid(), work, temp);

                if(lockf(fd, F_ULOCK, 0) < 0) perror("F_SETLOCK");
                close(fd);
                sleep(rand()%4);
            }
        }
    }

    for (int i = 0; i < n; i++) {
        waitpid(pid[i], &status, 0);
        exit(0);
    }
    return 0;
}
