#include <stdio.h>
#include <stdlib.h>
#include <dlfcn.h>

int main(int argc, char **argv){
  int a = 5,b = 10;
  void *handler;
  int (*powerfunc)(int a, int b);
  handler = dlopen("/home/noolk/Рабочий стол/eltex school/work/5/libs.so", RTLD_LAZY);
  if(!handler){
    fprintf(stderr, "dlopen() error %s\n", dlerror());
    exit(1);
  }
  powerfunc = dlsym(handler,argv[1]);
  printf("%d %s %d = %d\n", a, argv[1], b, (*powerfunc)(a,b));
  dlclose(handler);
  return 0;
}
